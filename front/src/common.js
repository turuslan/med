import { useContext, useEffect, useRef, useState } from 'preact/hooks'
import { createContext } from 'preact'

const isSysKey = e => e.key === 'Tab' || e.key === 'Shift' || e.key === 'Alt' || e.key === 'Control' || e.key === 'Meta' || e.altKey || e.ctrlKey || e.metaKey
const ignoreSysKeys = f => e => isSysKey(e) || f(e)

function useForce() {
  const [i, _i] = useState(0)
  return () => _i((i + 1) % 1000000)
}

class Comment {
  static OPEN = '[['
  static CLOSE = ']]'
  static split(s) {
    const r = []
    let i2 = 0
    while (true) {
      const i1 = s.indexOf(Comment.OPEN, i2)
      if (i1 === -1) {
        r.push([false, s.substring(i2)])  
        break
      }
      r.push([false, s.substring(i2, i1)])
      i2 = s.indexOf(Comment.CLOSE, i1 + Comment.OPEN.length)
      if (i2 === -1) {
        i2 = s.length
      } else {
        i2 += Comment.CLOSE.length
      }
      r.push([true, s.substring(i1, i2)])
    }
    return r
  }
}

function *enumerate(xs) {
  let i = 0
  for (const x of xs) {
    yield [i, x]
    ++i
  }
}

Commented.findRoot = e => {
  while (e && !e.hasAttribute('commented')) {
    e = e.parentNode
    if (e === document) e = null
  }
  return e
}
export function Commented({ children }) {
  if (typeof children !== 'string') {
    return 'Commented.Error'
  }
  return <span commented="">{Comment.split(children).map(([c, s]) => <span comment={c ? '' : null}>{s}</span>)}</span>
}

Input.findRoot = e => {
  while (e && !e.hasAttribute('input')) {
    e = e.parentNode
    if (e === document) e = null
  }
  return e
}
export function Input({ p, k, m, $e, v, $i, cref, onBlur, ...rest }) {
  function syncText(input, coms) {
    input.toggleAttribute('empty', coms.length === 1 && !coms[0][0] && !coms[0][1])
    for (const e of input.childNodes) if (e instanceof Text || e instanceof HTMLBRElement) e.remove()
    while (input.childNodes.length < coms.length) input.appendChild(document.createElement('span'))
    while (input.childNodes.length > coms.length) input.lastChild.remove()
    for (const [i, [c, t]] of enumerate(coms)) {
      const e = input.childNodes[i]
      e.toggleAttribute('comment', c)
      if (e.textContent !== t) e.textContent = t
    }
  }
  function getSel(input) {
    function getSel1(e, o) {
      if (input.parentNode === e && e.childNodes[o] === input) {
        e = input
        o = 0
      }
      while (e && e !== input) {
        while (e.previousSibling) {
          e = e.previousSibling
          o += (e.textContent || '').length
        }
        e = e.parentNode
      }
      return e ? o : null
    }
    const s = document.getSelection(), r = s.rangeCount && s.getRangeAt(0)
    if (r) {
      const o1 = getSel1(r.startContainer, r.startOffset), o2 = getSel1(r.endContainer, r.endOffset)
      if (o1 !== null && o2 !== null) {
        return [o1, o2]
      }
    }
    return null
  }
  function setSel(input, coms, sel) {
    function setSel1(x) {
      let o = 0, e
      for (const [i, [c, s]] of enumerate(coms)) {
        const next = o + s.length
        e = input.childNodes[i]
        if (x >= o && x <= next) {
          break
        }
        o = next
      }
      return e.firstChild ? [e.firstChild, Math.min(e.firstChild.textContent.length, x - o)] : [e, 0]
    }
    const s = document.getSelection(), r = document.createRange()
    r.setStart(...setSel1(sel[0]))
    r.setEnd(...setSel1(sel[1]))
    s.removeAllRanges()
    s.addRange(r)
  }
  const force = useForce()
  const [focus, setFocus] = useState(true)
  const d = useContext(Doc.Ctx)
  if (onBlur) {
    [v, $i] = useState(v)
  } else if (!$i && !onBlur) {
    v = d.i[k] || ''
    $i = t => { d.i[k] = t; v = t; d.u(k); force() }
  }
  const coms = Comment.split(v)
  const enable = ({ target }) => {
    if (d.r) return
    const input = Input.findRoot(target)
    if (!input.hasAttribute('contenteditable')) {
      input.setAttribute('contenteditable', '')
    }
    if (!getSel(input)) {
      setSel(input, coms, [v.length, v.length])
    }
  }
  const disable = ({ target }) => target.removeAttribute('contenteditable')
  const put = (input, p) => {
    if (!input.hasAttribute('contenteditable')) return
    const sel = getSel(input)
    let t = v.slice(0, sel[0]) + p + v.slice(sel[1])
    if (t.length && t[t.length - 1] === '\n') t += ' '
    const coms = Comment.split(t)
    syncText(input, coms)
    sel[0] = sel[1] = sel[0] + p.length
    setSel(input, coms, sel)
    $i(t)
  }
  return <span
    p={p || ' '}
    input=""
    tabindex={d.r ? null : '0'}
    onClick={e => { setFocus(true); enable(e) }}
    onMouseDown={() => setFocus(false)}
    onFocus={e => focus && enable(e)}
    onBlur={e => { setFocus(true); disable(e); onBlur && onBlur(v) }}
    ref={input => {
      if (!input) return
      const sel = getSel(input)
      syncText(input, coms)
      sel && setSel(input, coms, sel)
      cref && cref(input)
    }}
    onPaste={e => { e.preventDefault(); put(Input.findRoot(e.target), e.clipboardData.getData('text/plain').replace(m ? /[\t\r]+/g : /\n+/g, ' ')) }}
    onKeyDown={e => {
      const _e = (e.ctrlKey || e.metaKey) && $e
      if (isSysKey(e) && !_e) return
      if (e.key === 'Escape') { return e.target.blur() }
      if (e.key === 'Enter') {
        e.stopPropagation(); e.preventDefault()
        if (m && !_e) {
          put(e.target, '\n')
        } else {
          $e && $e(v)
        }
        return
      }
    }}
    onInput={e => $i(e.target.textContent)}
    {...rest}
  />
}

const nextTick = Promise.resolve()

export function Options({ t, k, kk, line }) {
  const _line = line ? '' : null
  const [w, setW] = useState(false)
  const _t = <label>
    <span><u><Commented>{t}</Commented></u></span>
    <input type="checkbox" checked={w} onClick={e => setW(e.target.checked)} />
  </label>
  const d = useContext(Doc.Ctx)
  let o = d.o[k] || (d.o[k] = { o: [], s: {}, a: '' })
  const force = useForce()
  if (kk) {
    const _o = o
    if (!_o.kkoc) _o.kkoc = {}
    k = `${k}::${kk}`
    o = d.o[k] || (d.o[k] = { o: null, s: {}, a: '', kk })
    o.kkop = _o; _o.kkoc[kk] = o
    o.o = _o.o
  }
  o.force = force
  function *group(self) {
    if (o.kkop) for (const _kk in o.kkop.kkoc) if (self || _kk !== kk) yield o.kkop.kkoc[_kk]
    else if (self) yield o
  }
  function isShared(v) {
    for (const _o of group(false)) if (_o.s[v] === true) return true
    return false
  }
  const f = o.o.filter(v => o.s[v] === true)
  const empty = f.length ? null : ''
  if (!w) {
    return <div options-empty={empty} options-line={_line}>
      {_t}
      {!f.length || <ul>{f.map(v => <li><Commented>{v}</Commented></li>)}</ul>}
    </div>
  }
  const [a, setA] = useState(o.a)
  o.a = a
  const add = () => {
    if (!a) { return }
    if (!o.o.includes(a)) {
      o.o.push(a)
      for (const _o of group(false)) _o.force()
    }
    force(o.s[a] = true)
    o.a = ''; setA('')
  }
  function firstLine(x) {
    const i = x.indexOf('\n')
    return i === -1 ? x : `${x.substr(0, i)} ...`
  }
  const [focusOn, setFocusOn] = useState(null)
  return <div options-empty={empty} options-line={_line}>
    {_t}
    <ul w="">{o.o.map((v, i) => <li key={v} u={o.s[v] !== true && ''}
      onClick={e => {
        if (d.r) return
        if (!document.getSelection().isCollapsed) return
        let t = e.target
        t = Input.findRoot(t) || t
        t = Commented.findRoot(t) || t
        let c = t
        c = c instanceof HTMLLIElement ? c.firstElementChild : c instanceof HTMLInputElement && c.type === 'checkbox' ? c : c.parentNode.previousElementSibling
        if (e.altKey) {
          if (isShared(v)) return
          c.indeterminate = true
          c.checked = false
          for (const _o of group(true)) _o.force(_o.s[v] = null)
          return
        }
        if (c !== document.activeElement && c.nextSibling.firstChild !== document.activeElement) c.click()
      }}
    >
      <input
        type="checkbox" checked={o.s[v] === true}
        indeterminate={o.s[v] === null}
        onChange={e => {
          if (e.target.indeterminate && !e.target.checked) return
          if (o.s[v] === null) for (const _o of group(false)) _o.force(_o.s[v] = false)
          o.s[v] = e.target.checked
          o.force()
        }}
      />
      <span>{o.s[v] === true ? <Input m v={v} onBlur={v2 => {
        if (v2 === v) return
        const dup = o.o.includes(v2), shared = isShared(v)
        if (shared) o.s[v] = false
        else delete o.s[v]
        o.s[v2] = true
        if (!dup || !shared) {
          if (dup) o.o.splice(i, 1)
          else if (shared) o.o.splice(i, 0, v2)
          else o.o[i] = v2
          for (const _o of group(false)) _o.force()
        }
        if (dup || shared) return setFocusOn(v2)
        force()
      }} cref={focusOn === null || v !== focusOn ? null : e => e && nextTick.then(() => {
        e.focus()
        setFocusOn(null)
      })} /> : <Commented>{firstLine(v)}</Commented>}</span>
    </li>)}</ul>
    <div style={{ paddingLeft: '1em' }}>
      <Input m p="(Добавить)" class="options-add-input" v={a} $i={setA} $e={add} />
      <span button="" onClick={add}>Добавить</span>
    </div>
  </div>
}

export function Select({ k, o, c, m }) {
  const other = '(другое)'
  const custom = v => typeof v !== 'number'
  const force = useForce()
  const d = useContext(Doc.Ctx)
  const _dsk = d.s[k], _dski = o.indexOf(_dsk)
  const selected = _dski === -1 ? _dsk === undefined ? 0 : _dsk : _dski
  const x_custom = custom(selected) && selected || ''
  const [_custom, __custom] = useState(x_custom)
  if (x_custom && _custom !== x_custom) {
    __custom(x_custom)
  }
  const _selected = v => { d.s[k] = custom(v) ? v : o[v]; d.u(k); force() }
  if (_dsk === undefined) _selected(0)
  const [popup, _popup] = useState(null)
  const [focus, _focus] = useState(false)
  const ref = useRef(null)
  const focusRef = focus ? e => { e && e.focus(); _focus(false) } : null
  const open = v => {
    if (d.r) return
    _popup(v); _focus(true)
  }
  const close = v => { _popup(null); _focus(true); _selected(v) }
  if (popup !== null) {
    const _text = v => custom(v) ? v || other : o[v]
    const row = (v, p) => <div
      popup={v === popup && ''}
      selected={v === selected && ''}
      onClick={e => { e.stopPropagation(); close(v) }}
      onMouseMove={() => _popup(v)}
      {...p}
    >{_text(v)}</div>
    const anchor = ref.current
    return <span
      select=""
      popup=""
      ref={ref}
    >
      <span>{_text(selected)}</span>
      <div
        tabindex="0"
        style={anchor && { left: `${Math.min(anchor.offsetLeft, anchor.offsetParent.offsetWidth - 200)}px`, top: `${anchor.offsetTop}px` }}
        ref={focusRef}
        onBlur={e => e.target.parentElement.hasAttribute('popup') && close(selected)}
        onKeyDown={ignoreSysKeys(e => {
          if (e.key === 'Escape') {
            e.target.blur()
          } else if (e.code === 'Space' || e.code === 'Enter') {
            e.preventDefault()
            e.stopPropagation()
            close(popup)
          } else if (e.key === 'ArrowRight' || e.key === 'ArrowDown' || e.code === 'KeyD' || e.code === 'KeyS') {
            e.preventDefault()
            _popup(custom(popup) ? popup !== '' ? '' : 0 : popup + 1 === o.length ? c ? _custom : 0 : popup + 1)
          } else if (e.key === 'ArrowLeft' || e.key === 'ArrowUp' || e.code === 'KeyA' || e.code === 'KeyW') {
            e.preventDefault()
            _popup(custom(popup) ? _custom !== '' && popup === '' ? _custom : o.length - 1 : popup === 0 ? c ? _custom !== '' ? '' : _custom : o.length - 1 : popup - 1)
          }
        })}
      >
        {o.map((v, i) => row(i, { key: i }))}
        {c && _custom !== '' && row(_custom, {})}
        {c && row('', {})}
      </div>
    </span>
  } else if (custom(selected)) {
    return <span
      select=""
      custom=""
      onClick={e => Input.findRoot(e.target) || open(selected)}
    >
      <Input
        v={selected}
        $i={_selected}
        p={other}
        m={m}
        $e={m || (() => open(selected))}
        cref={focusRef}
      />
    </span>
  }
  return <span
    select=""
    tabindex={d.r ? null : '0'}
    ref={focusRef}
    onKeyDown={ignoreSysKeys(e => e.key === 'Escape' ? e.target.blur() : open(selected))}
    onClick={() => open(selected)}
  >{custom(selected) ? selected : o[selected]}</span>
}

export function GroupSkipEmpty({ ks, children }) {
  const d = useContext(Doc.Ctx)
  const force = useForce()
  useEffect(() => {
    for (const k of ks) d.on(k, force)
    return () => {
      for (const k of ks) d.off(k, force)
    }
  })
  let skip = true
  for (const k of ks) {
    if (d.i[k]) {
      skip = false
      break
    }
  }
  return <span group-skip-empty='' skip={skip && ''} children={children} />
}

export class Doc {
  static P = 'doc:'
  static load(k) {
    const j = JSON.parse(localStorage.getItem(Doc.P + k))
    if (!j) { return null }
    const _o = {}
    for (const [k, { o: o0, s: _s, a, kk }] of Object.entries(j.o || {})) {
      const o = kk ? j.o[k.slice(0, -(2 + kk.length))].o : o0
      const s = {}
      for (const i of _s) { s[o[i]] = true }
      _o[k] = kk ? { kk, o, s, a } : { o, s, a }
    }
    const d = new Doc(k, j.i, _o, j.s || {})
    return d
  }
  constructor(k, i={}, o={}, s={}) {
    this.k = k
    this.i = i
    this.o = o
    this.s = s
    this.listeners = {}
  }
  save() {
    const j = { i: this.i, o: {}, s: this.s }
    for (const { o, s, kkoc, kkop } of Object.values(this.o)) {
      if (kkoc) for (const _o of Object.values(kkoc)) for (const [k, c] of Object.entries(_o.s)) if (c === null) {
        delete _o.s[k]
        s[k] = null
      }
      if (!kkop) for (const [k, c] of Object.entries(s)) if (c === null) {
        delete s[k]
        o.splice(o.indexOf(k), 1)
      }
    }
    for (const [k, { o, s: _s, a, kk }] of Object.entries(this.o)) {
      const s = []
      for (const [k, c] of Object.entries(_s)) if (c) s.push(o.indexOf(k))
      s.sort()
      j.o[k] = kk ? { kk, s, a } : { o, s, a }
    }
    localStorage.setItem(Doc.P + this.k, JSON.stringify(j))
  }
  on(k, f) {
    (this.listeners[k] = this.listeners[k] || []).push(f)
  }
  off(k, f) {
    const a = this.listeners[k], b = a ? a.indexOf(f) : -1
    if (b !== -1) a.splice(b, 1)
  }
  u = k => {
    const a = this.listeners[k]
    if (a) for (const f of a) f()
  }
  /** @type {Doc} _type */ static _type
  static Ctx = createContext(Doc._type)
}
